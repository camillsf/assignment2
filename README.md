# Assignment 2

This project is a file managements system that lets you: 
1. See all available files 
2. find files by their extension 
3. do file manipulation on file *Dracula.txt* that will be logged

The file-manipulations you can do are: 
1. see name of the file 
2. see size of the file 
3. see how many lines there are in the file
4. Search for words in the file 
    - search if a word exists in the file 
    - search how many times a word exist in the file 

All file-manipulations will be timed and logged.

To run the application go to the Assignment2 folder and run 
```
java -jar assignment2.jar
```

To see the log-file afterwards make sure you are still inn the Assignment2 folder and run 
`FileSystem\src\resources\Output\Logg.txt`





