import filemanager.*;
import java.util.Scanner;
import java.util.ArrayList;

public class Program {
    static Scanner scan;
    static FileInformation info;
    static Logger logger;

    public static void main(String[] args) {
        scan = new Scanner(System.in); //create input-reader
        info = new FileInformation(); //create File service
        logger = new Logger(); //create logging service
        boolean interactionKeeper = true;

        while (interactionKeeper) {
            //while loop to keep application running, and let user be able to choose what they want to do
            System.out.println("\n FILE SYSTEM MANAGER \n -------------------");
            System.out.print("1: Show available files ");
            System.out.print("\n2: Show files by extension");
            System.out.print("\n3: File manipulation on Dracula");
            System.out.print("\n4: Quit application \n");
            String input = scan.nextLine(); //read input from console
            try{
                int number = Integer.parseInt(input); //transform input from string to integer
                if (number == 1) {
                    showFiles();
                }else if (number == 2){
                    getExtensions();
                }else if (number == 3){
                   fileManipulation();
                }else if (number == 4){
                    interactionKeeper = false;
                }else{
                    System.out.println("You did not enter a valid number, enter a number in the range 1 to 4");
                }
            } catch (NumberFormatException ex) {
                System.out.println("You did not enter a number. Try again");
            }
        }
    }

    private static void showFiles(){
        info.showResourceFiles(); //call on method to show all resourceFiles
        boolean interactionKeeper = true;

        while (interactionKeeper){
            //while loop to keep user able to see filenames as long as they want.
            System.out.println("Enter q to go back");
            String input = scan.nextLine();
            if (input.equals("q")){
                interactionKeeper = false;
            }else {
                System.out.println("You did not enter a valid input. Try again");
            }
        }
    }


    private static void getExtensions(){
        boolean interactionKeeper = true;

        while(interactionKeeper) {
            //While loop to let user keep seeing available extensions until they are ready to choose extension or go back.
            ArrayList<String> extensions = info.showExtensions(); //call on method to show available extensions and get arraylist of extensions.
            int numberOfExtensions = extensions.size(); //get number of elements in arrayList
            System.out.println("Enter q to go back");
            String input = scan.nextLine(); //read user input from console
            if (input.equals("q")){
                interactionKeeper = false;
            }else{
                try {
                    int number = Integer.parseInt(input); //convert input to number
                    if (number > 0 && number <= numberOfExtensions) { //check if number is a number between 1 and number of elements in arraylist
                        String ext = extensions.get(number - 1); //look up which extension user has asked for by accessing index number in arraylist
                        info.getFilesByExtension(ext); //call on function to show all files with that extension
                        boolean newInteractionKeeper = true;

                        while (newInteractionKeeper){
                            //while loop to keep user able to see files as long as they want.
                            System.out.println("Enter q to go back");
                            String newInput = scan.nextLine();
                            if (newInput.equals("q")){
                                newInteractionKeeper = false;
                            }else{
                                System.out.println("You did not enter a valid input. Try again");
                            }
                        }
                    } else { //user entered invalid number
                        System.out.println("You did not enter a valid number, enter a number in the range 1 to " + numberOfExtensions);
                    }
                } catch (NumberFormatException ex) {
                    System.out.println("You did not enter a number. Try again");
                }
            }
        }
    }


    private static void fileManipulation(){
        boolean interactionKeeper = true;

        while (interactionKeeper){
            //while loop to let user be able to choose what file-manipulation option they want to do
            System.out.println("\nFILEMANIPULATION \n----------------");
            System.out.print("1: show filename");
            System.out.print("\n2: show filesize");
            System.out.print("\n3: show number of lines");
            System.out.print("\n4: word count ");
            System.out.print("\nEnter q to go back \n");
            String input = scan.nextLine(); //read input from user
            if (input.equals("q")){
                interactionKeeper = false;
            }else{
                try {
                    int number = Integer.parseInt(input);//convert input to integer.
                    if (number == 1){
                        logger.begin(); //start logging service
                        String name = info.showName("Dracula.txt"); //call on function to show filename to user
                        logger.end(name); //end logging service
                        boolean newInteractionKeeper = true;

                        while (newInteractionKeeper) {
                            //while loop to let user keep seeing the filename until they want to go back.
                            System.out.println("Enter q to go back");
                            String newInput = scan.nextLine();
                            if (newInput.equals("q")) {
                                newInteractionKeeper = false;
                            } else {
                                System.out.println("You did not enter a valid input. Try again");
                            }
                        }
                    }else if (number == 2){
                        logger.begin();//start logging service
                        String fileSize = info.showSize("Dracula.txt");//call on function to show size of file to user
                        logger.end(fileSize);//end logging service
                        boolean newInteractionKeeper = true;

                        while (newInteractionKeeper) {
                            //while loop to let user keep seeing the size of file until they want to go back.
                            System.out.println("Enter q to go back");
                            String newInput = scan.nextLine();
                            if (newInput.equals("q")) {
                                newInteractionKeeper = false;
                            } else {
                                System.out.println("You did not enter a valid input. Try again");
                            }
                        }
                    }
                    else if (number == 3){
                        logger.begin(); //start logging service
                        String linecount = info.showLineCount("Dracula.txt");//call on function to show line count to user
                        logger.end(linecount);//end logging service
                        boolean newInteractionKeeper = true;

                        while (newInteractionKeeper) {
                            //while loop to let user keep seeing the number of lines until they want to go back.
                            System.out.println("Enter q to go back");
                            String newInput = scan.nextLine();
                            if (newInput.equals("q")) {
                                newInteractionKeeper = false;
                            } else {
                                System.out.println("You did not enter a valid input. Try again");
                            }
                        }
                    }else if (number == 4){
                        boolean interactionKeeper1 = true;

                        while (interactionKeeper1){
                            //while loop to let user be able to choose what type of search they want to do
                            System.out.println("\nWORD SEARCH \n----------- ");
                            System.out.print("1: search if a word exists in file");
                            System.out.print("\n2: search how many times a word is found in file");
                            System.out.print("\nEnter q to go back\n");
                            String newInput = scan.nextLine(); //read input from console

                            if(newInput.equals("q")){
                                interactionKeeper1 = false;
                            }else {
                                try {
                                    int newNumber = Integer.parseInt(newInput); //convert input to int
                                    if (newNumber == 1) {
                                        boolean interactionKeeper2 = true;

                                        while (interactionKeeper2){
                                            //while loop to let user keep entering what word they want to search for or choose to go back
                                            System.out.println("Enter the word you want to search for");
                                            System.out.println("Enter q to go back");
                                            String searchWord = scan.nextLine(); //read input from console

                                            if (searchWord.equals("q")){
                                                interactionKeeper2 = false;
                                            }
                                            else{
                                                logger.begin();//start loggig service
                                                String wordSearch = info.showWordSearch("Dracula.txt", searchWord); //call on method to search for word with the user input
                                                logger.end(wordSearch); //end logging service
                                            }
                                        }
                                    } else if (newNumber == 2) {
                                        boolean interactionKeeper2 = true;

                                        while (interactionKeeper2){
                                            //while loop to let user keep entering what word they want to search for or choose to go back
                                            System.out.println("Enter the word you want to search for");
                                            System.out.println("Enter q to go back");
                                            String searchWord = scan.nextLine();//read input from console
                                            if (searchWord.equals("q")){
                                                interactionKeeper2 = false;
                                            }
                                            else{
                                                logger.begin();//start logging service
                                                String WordCountSearch = info.showWordSearchCount("Dracula.txt", searchWord);//call on method to search for word-count with the user input
                                                logger.end(WordCountSearch);//end logging service
                                            }
                                        }
                                    } else {
                                        System.out.println("You did not enter a valid number, enter a number in the range 1 to 2");
                                    }
                                } catch (NumberFormatException ex) {
                                    System.out.println("You did not enter a number. Try again");
                                }
                            }
                        }
                    }
                    else{
                        System.out.println("You did not enter a valid number, enter a number in the range 1 to 4");
                    }
                }
                catch (NumberFormatException ex) {
                    System.out.println("You did not enter a number. Try again");
                }
            }
        }
    }


}