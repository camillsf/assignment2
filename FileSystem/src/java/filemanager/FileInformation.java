package filemanager;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileInformation{
    File [] listOfFiles;
    String currentDirectory;
    File pathToFiles;


    public FileInformation(){
        currentDirectory = System.getProperty("user.dir"); //Users current working-directory.
        pathToFiles = new File(currentDirectory + "/FileSystem/src/resources/Files"); //File from path to Files folder.
        listOfFiles = pathToFiles.listFiles(); //Array containing all the files in resource/Files folder.
    }


    public void showResourceFiles(){
        //Iterates through array containing all resource files to find filenames and print filenames to console.
        if (listOfFiles != null){
            int numberOfFiles = listOfFiles.length;
            System.out.println("\nAVAILABLE FILES \n --------------- ");

            for (int i = 0; i < numberOfFiles; i++){
                File currentFile = listOfFiles[i];
                String name = currentFile.getName();
                System.out.println(name);
            }
        }
        else {
            System.out.println("No files found.");
        }
    }


    public ArrayList<String> showExtensions(){
        //Iterates through array with resource files to find all extensions.
        // Prints all extensions to console and returns them in an arrayList.
        ArrayList<String> extensions = new ArrayList<String>();//Initiates arrayList that is to be returned.
        Pattern regexPattern = Pattern.compile("(\\..*)$"); //Regular expression to find file-extensions.

        for (int i = 0; i < listOfFiles.length; i++){ //for each file in array of files
            String filename = listOfFiles[i].getName(); //extract filename
            Matcher m = regexPattern.matcher(filename); //match filename with regex to find extension
            if (m.find()){ //if there were a match to regex
                String ext = m.group(1); //Group 1 is the regex capture group with the extension
                if (! extensions.contains(ext)){ //Checking if the extension already exists in the arrayList
                    extensions.add(ext); //If not in the list, we add it to the list.
                }
            }
        }
        System.out.println("\nCHOOSE EXTENSION \n---------------- ");
        for (int i = 1; i <= extensions.size() ; i++){
            //iterates through arrayList of all extesions to print extension to console with index plus one in the front.
            String ext = extensions.get(i-1);
            System.out.println(i + ": " + ext);
        }
        return extensions; //returns arrayList of all extensions.
    }


    public ArrayList<File> getFilesByExtension(String ext){
        //Takes in a string of an extension, iterates through array and finds all files with the same extension.
        //Prints all files matching the extension to console, and returns an arrayList of the files matching the extension.
       ArrayList<File> filesWithExtension = new ArrayList<File>();//Initiates arrayList that is to be returned.
       Pattern regexPattern = Pattern.compile("(\\..*)$");//Regular expression to find file-extensions

       for (int i = 0; i < listOfFiles.length; i++){ //for all files in array of files
           String filename = listOfFiles[i].getName(); //get name
           Matcher m = regexPattern.matcher(filename); //match filename to regex to find extension
           if (m.find()){ //if there were a match to the regex
               String extension = m.group(1);//get extension
               if (extension.equals(ext)){ //if the file extension matches the inputVariable extesion
                   filesWithExtension.add(listOfFiles[i]); //add file to arrayList.
               }
           }
       }
       System.out.println("\nFILES WITH EXTENSION " + ext + "\n--------------------");
       for (File f : filesWithExtension){
           //iterates through arrayList of matching extensions and prints filename to console.
           System.out.println(f.getName());
       }
       return filesWithExtension; //return arraylist.
    }


    public String showName(String filename){
        //Takes in string with filename, finds file in resources and gets filename.
        //Prints returnstatement containing filename to console, as well as returning the returnstatement for it to be logged.
        String pathToFile = pathToFiles + "/" + filename;
        File dracula = new File(pathToFile); //File from path to file
        String name = dracula.getName(); //get name
        String returnstatement = "Filename is " + name + ".";
        System.out.println(returnstatement);
        return returnstatement;
    }


    public String showSize(String filename){
        //Takes in a string containing filename, finds file in resources and reads file byte by byte and counts number of bytes.
        //Prints returnstatement containing number of kilobytes, as well as returning the returnstatement so it can be logged.
        String returnStatement = "";
        String pathToFile = pathToFiles + "/" + filename;
        try(FileInputStream stream = new FileInputStream(pathToFile)){ //Try to open Fileinputstream from path to file
            int currentByte = stream.read(); //first byte
            int byteCounter = 1; //Initiates counter to one since we are reading the first byte

            while (currentByte != -1){ //while there are bytes to read
                byteCounter++; //increase counter
                currentByte = stream.read(); //read next byte
            }
            returnStatement = "File " + filename + " is " + byteCounter/1024.0 + " kilobytes.";
            System.out.println(returnStatement);
        }
        catch(IOException ex){ //throws exception if not possible to open Fileinputstream from path to file.
            System.out.println(ex.getMessage());
        }
        return returnStatement;
    }

    public String showLineCount(String filename){
        //Takes in a string containing filename, finds file in resources and reads file line by line and counts number of lines.
        //Prints returnstatement containing number lines, as well as returning the returnstatement so it can be logged.
        String returnStatement = "";
        String pathToFile = pathToFiles + "/" + filename;
        try(BufferedReader buffer = new BufferedReader(new FileReader(pathToFile))){ //Try to open BufferReader from path to file
            String line;
            int lineCounter = 0;

            while ((line = buffer.readLine()) != null){ //while buffer.readLine() is not null we read Line from file
                lineCounter++; //increase counter
            }
            returnStatement = ("File " + filename + " has " + lineCounter + " number of lines.");
            System.out.println(returnStatement);
        }
        catch(IOException ex){ //If not possible to open BufferReader from path to file throw exception
            System.out.println(ex.getMessage());
        }
        return returnStatement;
    }


    public String showWordSearch(String filename, String word){
        //Takes in a String filename and a String word and reads file word by word to see if the word exists in the file.
        //Prints returnstatement saying wheter or not the word was found, and returns the returnstatement for it to be logged.
        String returnStatement = "";
        String pathToFile = pathToFiles + "/" + filename;
        try(Scanner scanner = new Scanner(new File(pathToFile))){ //Try to open scanner from path to file.
            scanner.useDelimiter(" "); //Use a space as delimiter to read file word by word
            String fileWord;
            boolean notFound = true; //boolean to keep track of wheter the word is found or not.

            while (scanner.hasNext() && notFound){ //while there is a next word, and the word is not found
                fileWord = scanner.next(); //read next word from file
                if ((word.toLowerCase()).equals(fileWord.toLowerCase())){ //check if the word from file matches the provided word. Converting both word to lower case to ignore case
                    notFound = false; //if they match notFound is set to false.
                }
            }
            if (notFound){
                returnStatement = "The word " + word + " does not exist in " + filename + ".";
            }else {
                returnStatement = "The word " + word + " does exist in " + filename + ".";
            }
            System.out.println(returnStatement);
        }
        catch(IOException ex){//Throws exception if not possible to open scanner from file
            System.out.println(ex.getMessage());
        }
        return returnStatement;
    }

    public String showWordSearchCount(String filename, String word){
        //Takes in a String filename and a String word and reads file word by word to see how many times the word exists in the file.
        //Prints returnstatement saying how many times the word was found, and returns the returnstatement for it to be logged.
        String returnStatement = "";
        String pathToFile = pathToFiles + "/" + filename;
        try(Scanner scanner = new Scanner(new File(pathToFile))){//Try to open scanner from path to file.
            scanner.useDelimiter(" ");//Use a space as delimiter to read file word by word
            String fileWord;
            int wordCounter = 0;

            while (scanner.hasNext()){//while there is a next word to read
                fileWord = scanner.next(); //read next word
                if ((word.toLowerCase()).equals(fileWord.toLowerCase())){ //checks if word read from file is the same as the provided word, ignoring case
                    wordCounter++;
                }
            }
            if (wordCounter > 0){
                returnStatement = "The word " + word + " was found " + wordCounter + " times in " + filename;
            }else {
                returnStatement = "The word " + word + " does not exist in " + filename;
            }
            System.out.println(returnStatement);
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        return returnStatement;
    }


}