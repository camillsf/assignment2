package filemanager;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.Duration;

public class Logger{
    String currentDirectory;
    File outputFile;
    LocalDateTime starttime;
    LocalDateTime endtime;

    public Logger(){
        currentDirectory = System.getProperty("user.dir"); //gets users current directory.
        outputFile = new File(currentDirectory + "/FileSystem/src/resources/Output/Logg.txt"); // Create File from path to logging-file.
        starttime = LocalDateTime.now(); //initiates starttime to the current time.
        endtime = LocalDateTime.now(); //initiates endtime to the current time.

        try(FileWriter writer = new FileWriter(outputFile)){
            writer.close();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        //Open the logg so that it clears of previous text, and close it again.
    }


    public void begin(){
        starttime = LocalDateTime.now(); //sets starttime to the current time.
    }


    public void end(String message){
        endtime = LocalDateTime.now(); //sets endtime to the current time.
        long executionTime = Duration.between(starttime, endtime).toMillis(); //calculate the difference between starttime and endtime.

        try(FileWriter writer = new FileWriter(outputFile, true)){
            writer.write(starttime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + " " + message + ". The function took " + executionTime + " milliseconds to execute.\n");
            writer.close();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        //Open the logg in append mode, and write starttime, the message printed to console plus the execution time.
    }
}